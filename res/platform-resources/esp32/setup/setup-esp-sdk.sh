#!/bin/bash

######## Setup ESP-IDF(SDK)	

export CURR_DIR=`pwd`

#create ESP directory in $HOME
if [ ! -z "$1" ]
then
	ESP_DIR_PATH=$1	
else
	ESP_DIR_PATH=$CURR_DIR
fi

ESP_IDF_PATH=$ESP_DIR_PATH/esp-idf


# DO NOT CHANGE, this is the Checkout id for "v4.1-dev-256-g9f145ff"
ESP_IDF_CHECKOUT_ID=9f145ff165099f32b92b7ee022a1debc5f0a583d

mkdir -p $ESP_DIR_PATH

#change directory to $ESP_DIR_PATH
cd $ESP_DIR_PATH

echo "### Starting ESP-IDF SDK Download #########################"
git clone --recursive https://github.com/espressif/esp-idf.git

LAST_CMD_RETURN_STAT=$?

if [[ $LAST_CMD_RETURN_STAT -eq 0 ]]
then
	cd $ESP_IDF_PATH

	#git describe --tags --dirty

	#git branch

	#git checkout v4.1-dev-256-g9f145ff"

	git checkout $ESP_IDF_CHECKOUT_ID

	git describe --tags --dirty

else
	echo "### Failed to Download ESP-IDF SDK #########################"
	echo
	exit $EXECUTION_STAT
fi

echo "### Completed ESP-IDF SDK Download #########################"
echo
echo

echo "### Setup SDK Environment Variable #########################"
echo "Please follow the below instructions to set the ESP-IDF SDK environment variables"
echo
echo "Step 1: Set up IDF_PATH by adding the following line to ~/.profile file:"
echo
echo -e "	\e[38;5;0;104;1m export IDF_PATH=$ESP_DIR_PATH/esp-idf \e[0m"
echo
echo "Step 2: Log off and log in back to make this change effective. Run the following command to check if IDF_PATH is set:"
echo
echo -e "	\e[38;5;0;104;1m printenv IDF_PATH \e[0m"
echo






