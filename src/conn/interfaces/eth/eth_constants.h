/*
 * File Name: eth_constants.h
 * File Path: /emmate/src/conn/interfaces/eth/eth_constants.h
 * Description:
 *
 *  Created on: 24-May-2019
 *      Author: Rohan Dey
 */

#ifndef ETH_CONSTANTS_H_
#define ETH_CONSTANTS_H_

#define ETH_MAC_LEN			6		/**< Length in bytes of a Ethernet module's MAC */

#endif /* ETH_CONSTANTS_H_ */
