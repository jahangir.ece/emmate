# Connectivity Module

## Overview

The connectivity module helps the application to connect to a network. This module features easy to use APIs to initialize, connect and disconnect to/from a network. The connectivity module supports multiple network interfaces and network protocols for an application to use.

## Features

The connectivity module supports the following Network Interfaces and Protocols

#### Supported Network Interfaces

1. Wi-Fi - For more details see [Details of Wi-Fi Module](wifi/wifi.md)
2. Ethernet - *(not yet implemented)*
3. Bluetooth Low Energy (BLE) - For more details see [Details of BLE Module](ble/ble.md)
4. GSM - *(not yet implemented)*
5. NB-IoT - *(not yet implemented)*

**For more details of each interface please see its Readme, API Documentation and Examples**

#### Supported Protocols

1. HTTP(S) - For more details see [Details of HTTP Module](http/http.md)
2. SNTP - For more details see [Details of SNTP Module](sntp/sntp.md)

**For more details of each protocol please see its Readme, API Documentation and Examples**


## How to use the Connectivity Module from an Application

The APIs exposed from the connectivity module are provided in `conn.h`

#### Header(s) to include

```
conn.h
```

#### Sample APIs

The below sample code will show how to initialize and connect to a Wi-Fi network. To be able to do so, we need to set the configuration `IFACE_WIFI` to 1.

1. Initializing a network.

```
/* Pre-requisite: IFACE_WIFI have to be set to 1 */

DeviceConfig dev_cfg;

strcpy(dev_cfg.wifi_cfg.wifi_ssid, "MY-SSID");
strcpy(dev_cfg.wifi_cfg.wifi_pwd, "PASSWORD");

core_err ret = init_network(&dev_cfg);
	if (ret != CORE_OK) {
	CORE_LOGE(TAG, "Failed to initialize the network");
}
```

1. Connecting to a network.

```
/* Pre-requisite: IFACE_WIFI have to be set to 1. */

ret = connect_to_network(&dev_cfg);
if (ret != CORE_OK) {
	CORE_LOGE(TAG, "Failed to connect to network");
	return ret;
}
```