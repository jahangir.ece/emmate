/*
 * appconfig_helper_status.h
 *
 *  Created on: 16-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef APPCONFIG_HELPER_STATUS_H_
#define APPCONFIG_HELPER_STATUS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"


core_err _send_appconfig_status_to_server(int config_stat);

#ifdef __cplusplus
}
#endif

#endif /* APPCONFIG_HELPER_STATUS_H_ */
