/*
 * hw_identify.c
 *
 *  Created on: 29-Jul-2019
 *      Author: Rohan Dey
 */

#include "hw_identify.h"
#include "hw_identify_parser.h"
#include "board_ids.h"
#include "http_client_api.h"
#include "http_client_core.h"
#include "http_constant.h"
#include "threading.h"
#include "module_thread_priorities.h"
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif
#include "system.h"
#include "system_urls.h"
#include "system_utils.h"
#include "event_group_core.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "core_utils.h"
#include <string.h>

#define TAG		LTAG_HWIDENTIFY

typedef struct {
	ThreadHandle hwid_thread;
} HWIdentifyData;

static HWIdentifyData hwid;

#define HWID_HTTP_RESPONSE_SIZE		200
//#define HWID_MAX_RETRY				5000
#define HWID_DELAY_CHANGE_COUNT		100
#define RETRY_DELAY_MULTIPLIER		2

static core_err make_hwid_data(HWIdentificationRequest *hwid_req, char **json_buf, int *json_len) {
	core_err ret = CORE_FAIL;

	/* Get all available network interface's physical address */
#if CONFIG_USE_WIFI
	get_board_wifi_sta_mac(hwid_req->wifi_mac);
#endif
#if CONFIG_USE_ETH
#endif
#if CONFIG_USE_GSM
#endif
#if CONFIG_USE_BLE
	get_board_bt_mac(hwid_req->bt_mac);
#endif

	ret = make_hwid_json(json_buf, json_len, hwid_req);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "make_hwid_json failed!");
	}
	return ret;
}

static core_err validate_hwid_response(HWIdentificationResponse *hwid_resp) {
	core_err ret = CORE_FAIL;

	if (hwid_resp->status) {
		if ((strlen(hwid_resp->somthing_id) > 0)/* || (strlen(hwid_resp->custom_hw_id) > 0)*/)
			ret = CORE_OK;
		else
			ret = CORE_FAIL;
	}

	return ret;
}

static core_err save_hwid_response(HWIdentificationResponse *hwid_resp) {
	core_err ret = CORE_FAIL;
#if CONFIG_USE_PERSISTENT

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)

	if (strlen(hwid_resp->somthing_id) > 0) {
		set_virtual_somthing_id(hwid_resp->somthing_id);

		/* Save somthing_id into Persistent Memory */
		if (((ret = write_somthing_id_to_persistent_mem(hwid_resp->somthing_id))) == CORE_OK) {
		} else {
			CORE_LOGE(TAG, "save_hwid_response somthing_id failed!");
		}
	} /*else if (strlen(hwid_resp->custom_hw_id) > 0) {
	 set_virtual_som_id(hwid_resp->custom_hw_id);

	 Save custom_hw_id into Persistent Memory
	 if (((ret = write_somthing_id_to_persistent_mem(hwid_resp->custom_hw_id))) == CORE_OK) {
	 } else {
	 CORE_LOGE(TAG, "save_hwid_response custom_hw_id failed!");
	 }
	 }*/
#elif (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC)
	// TODO: Save data in file
#endif

#endif
	return ret;
}

static void perform_hardware_identification(void * params) {
	core_err ret;
	int hwid_retry = 0;
	int retry_time_delay = 5;	// Start the retry time delay with 5 secs
	bool hwid_done = false, retry_http_oper = false;
	char *http_response = NULL;
	size_t http_response_len = 0;

	CORE_LOGD(TAG, "Waiting for network IP ...");
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

	while (1) {
		/* Make Hardware Identification JSON */
		CORE_LOGD(TAG, "Starting Hardware Identification ...");

		/* Make the Hardware Identification request data to be posted to server */
		char *json_buf;
		int json_len;
		HWIdentificationRequest hwid_req;
		memset(&hwid_req, 0, sizeof(HWIdentificationRequest));
		ret = make_hwid_data(&hwid_req, &json_buf, &json_len);
		CORE_LOGW(TAG, "Request: %.*s", json_len, json_buf);

		/* Initialize a Hardware Identification response data to be populated after receiving the response from server */
		HWIdentificationResponse hwid_resp;
		memset(&hwid_resp, 0, sizeof(HWIdentificationResponse));

		/* Allocate memory for the http response */
		http_response = (char*) calloc(HWID_HTTP_RESPONSE_SIZE, sizeof(char));
		if (http_response == NULL) {
			CORE_LOGE(TAG, "memory allocation for http_resp_data failed");
			retry_http_oper = true;
			goto free_memory;
		}
		memset(http_response, 0, HWID_HTTP_RESPONSE_SIZE);
		http_response_len = 0;

		/* Do http operation */
		uint16_t http_stat = 0;
		ret = do_http_operation(IQ_SYS_HWIDENTIFY_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
		IQ_HOST, HTTP_USER_AGENT,
		CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_response, &http_response_len, HWID_HTTP_RESPONSE_SIZE, &http_stat);

		if (ret == CORE_OK) {
			CORE_LOGW(TAG, "Response: %.*s", http_response_len, http_response);

			/* Parse the http response data */
			CORE_LOGD(TAG, "Going to parse Hardware Identification response");

			/* If we have a SOM ID, be it physical or virtual do normal process */
			ret = parse_hwid_response_json(http_response, &hwid_resp);

			/* Check the return value of the parser */
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Hardware Identification response parsing failed! Free memory and try again!");
				retry_http_oper = true;
				goto free_memory;
			} else {
				/* Print the sys heartbeat response values received from the server */
				print_hwid_response(&hwid_resp);

				/* Check and validate the response data */
				CORE_LOGD(TAG, "Going to validate the Hardware Identification response");
				ret = validate_hwid_response(&hwid_resp);
				if (ret == CORE_OK) {
					/* Save the virtual hardware id */
					save_hwid_response(&hwid_resp);
					hwid_done = true;
					hwid_retry = 0;
				} else {
					retry_http_oper = true;
					goto free_memory;
				}
			}
		} else {
			retry_http_oper = true;
			CORE_LOGE(TAG, "HTTP failed with status code = ( %d )!", http_stat);
//			hwid_retry++;
//			if (hwid_retry != HWID_MAX_RETRY) {
//				TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
//			} else
//				CORE_LOGE(TAG, "Retry max count reached!");
		}

		free_memory:
		/* Free the allocated http respose memory */
		CORE_LOGD(TAG, "Freeing allocated response memory");
		free(json_buf);
		free(http_response);

		/* Check if the job is done */
		if (hwid_done)
			break;

		if (retry_http_oper) {
			retry_http_oper = false;
			/* Every HWID_DELAY_CHANGE_COUNT times we increment the time delay in between retries */
			if ((++hwid_retry % HWID_DELAY_CHANGE_COUNT) == 0) {
				retry_time_delay = retry_time_delay * RETRY_DELAY_MULTIPLIER;
				CORE_LOGI(TAG, "Setting retry_time_delay to %d", retry_time_delay);
			}
//			if (++hwid_retry < HWID_MAX_RETRY) {
//				CORE_LOGW(TAG, "Retrying in %d secs... Retry Count = %d", (DELAY_5_SEC/1000), hwid_retry);
//			} else
//				CORE_LOGE(TAG, "Retry max count reached!");
		}

//		/* Check for retry */
//		if (hwid_retry >= HWID_MAX_RETRY) {
//			CORE_LOGE(TAG, "Max retry of %d times over. Killing hw-identify task", hwid_retry);
//			/* TODO: error handling and reporting */
//			break;
//		}

//		event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
		TaskDelay((retry_time_delay * 1000) / TICK_RATE_TO_MS);
	}

	if (hwid_done) {
		/* The sys heartbeat module should be waiting on this bit, so set it before exiting the thread */
		event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}

	/* Kill the task */
	TaskDelete(NULL);
}

core_err start_hardware_identification() {
	core_err ret = CORE_FAIL;

	/* Create a thread to perform hardware identification */
	BaseType thread_stat;
	thread_stat = TaskCreate(perform_hardware_identification, "hw-identify", TASK_STACK_SIZE_6K, NULL,
			THREAD_PRIORITY_HWID, &hwid.hwid_thread);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create thread!");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}

	return ret;
}
