/*
 * File Name: sysconfig_helper.c
 * File Path: /emmate/src/system/sysconfig/sysconfig_helper.c
 * Description:
 *
 *  Created on: 12-May-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include <stdarg.h>

#include "sysconfig_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_SYSTEM_SYSCFG

static gll_t *dclist = NULL;
static SYSCONFIG_STATUS sysconfig_stat = SYSCONFIG_FREE;

SYSCONFIG_STATUS get_sysconfig_stat() {
	return sysconfig_stat;
}

core_err init_sysconfig(int key_count, ...) {

	core_err ret = CORE_FAIL;

	if (sysconfig_stat == SYSCONFIG_BUSY) {
		CORE_LOGE(TAG, "Could not initialize the system configuration module because it is already in use!");
		return CORE_FAIL;
	}

	sysconfig_stat = SYSCONFIG_BUSY;

	/* create linked list node */
	if (dclist == NULL) {
		dclist = gll_init();
		if (dclist == NULL) {
			CORE_LOGE(TAG, "Could not allocate memory for System Configurator\n");
			return CORE_ERR_NO_MEM;
		}
	}

	va_list valist;

	/* initialize valist for number of arguments */
	va_start(valist, key_count);

	for (int i = 0; i < key_count; i++) {
		/* Get the next key from the list */
		char *key = va_arg(valist, char*);
		int key_len = strlen(key);

		CORE_LOGI(TAG, "%d) key = %s, len = %d\n", i, key, key_len);

		/* allocate memory for key */
		char *ptr = (char*) calloc((key_len + 1), sizeof(char));
		if (ptr == NULL) {
			CORE_LOGE(TAG, "Could not allocate memory for key = %s\n", key);
			ret = CORE_ERR_NO_MEM;
			break;
		} else {
			strcpy(ptr, key);
			SysconfigData *scdata = (SysconfigData*) calloc(1, sizeof(SysconfigData));
			if (scdata != NULL) {
				scdata->key = ptr;
				CORE_LOGD(TAG, "%d) adding list node at position %d\n", i, dclist->size);
				CORE_LOGD(TAG, "%d) node: key = %s %d\n", i, scdata->key, scdata->value);
				gll_add(dclist, scdata, dclist->size);
				ret = CORE_OK;
			} else {
				CORE_LOGE(TAG, "Could not allocate memory for SysconfigData!");
				ret = CORE_ERR_NO_MEM;
				break;
			}
		}
	}

//cleanup:
	if (ret == CORE_ERR_NO_MEM) {
		CORE_LOGE(TAG, "Error occured while initializing System Config, cleaning up memory");
		deinit_sysconfig();
	}
	/* clean memory reserved for valist */
	va_end(valist);

	return ret;
}

core_err update_sysconfig_metakey(char *oldkey, char *newkey) {
	core_err ret = CORE_FAIL;
	SysconfigData *scdata = NULL;

	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);

		if(strcmp(scdata->key, oldkey) == 0) {
			CORE_LOGD(TAG, "found key = %s, having old value = %d\n", scdata->key, scdata->value);
			CORE_LOGD(TAG, "updating new key = %s\n", newkey);
			scdata->key = newkey;
			return CORE_OK;
		}
	}
	CORE_LOGI(TAG, "could not find key = %s\n", oldkey);

	return ret;
}

core_err update_sysconfig_metavalue(char *oldkey, int newvalue) {
	core_err ret = CORE_FAIL;
	SysconfigData *scdata = NULL;

	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);

		if(strcmp(scdata->key, oldkey) == 0) {
			CORE_LOGD(TAG, "found key = %s, having old value = %d\n", scdata->key, scdata->value);
			CORE_LOGD(TAG, "updating new value = %d\n", newvalue);
			scdata->value = newvalue;
			return CORE_OK;
		}
	}
	CORE_LOGI(TAG, "could not find key = %s\n", oldkey);

	return ret;
}

void print_sysconfig_list() {
	SysconfigData *scdata = NULL;
	CORE_LOGI(TAG, "\n============== PRINTING METADATA LIST ==============\n");
	CORE_LOGI(TAG, "meta list size is = %d\n", dclist->size);
	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);
		if (scdata != NULL) {
			CORE_LOGI(TAG, "%d) key = %s,\tvalue = %d\n", i, scdata->key, scdata->value);
		} else {
			CORE_LOGE(TAG, "%d) meta is NULL\n", i);
		}
	}
	CORE_LOGI(TAG, "\n\n");
}

void deinit_sysconfig() {
	SysconfigData *scdata = NULL;

	CORE_LOGD(TAG, "meta list size is = %d\n", dclist->size);
	for (int i = 0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);
		if (scdata != NULL) {
			free(scdata->key);
			free(scdata);
		} else {
			CORE_LOGE(TAG, "%d) meta is NULL\n", i);
		}
	}
	gll_destroy(dclist);
	sysconfig_stat = SYSCONFIG_FREE;
}
