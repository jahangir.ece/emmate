/*
 * File Name: sntp_client_platform.c
 * File Path: /emmate/src/platform/esp/conn-proto/sntp-client/sntp_client_platform.c
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#include "sntp_client_platform.h"

#define TAG "SNTP"

void initialize_sntp_platform(const char *sntp_server)
{
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, (char*)sntp_server);
    sntp_init();
}
