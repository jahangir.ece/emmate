/*
 * ble_platform.h
 *
 *	File Path: /emmate/src/conn/ble/esp-idf/ble_platform.h
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#ifndef SRC_CONN_BLE_ESP_IDF_BLE_PLATFORM_H_
#define SRC_CONN_BLE_ESP_IDF_BLE_PLATFORM_H_

#include "ble_platform_constant.h"
#include "gatt_utils.h"
#include "core_error.h"

typedef BLE_RECV_DATA 	BLE_DATA;

typedef void(*BLE_CONNECT_EVENT)(BLE_CONNECTION_STATUS status);


/**
 * @brief
 *
 * @return
 *
 **/
uint8_t ble_platform_get_ble_max_name_size();

/**
 * @brief
 *
 * @return
 *
 **/
core_err ble_platform_set_ble_adv_name(char* pble_adv);

/**
 * @brief		Config ESP32 Ble Module
 *
 * @return
 *
 **/
core_err ble_platform_config_ble();

/**
 * @brief		de_Config ESP32 Ble Module
 *
 * @return
 *
 **/
core_err ble_platform_deconfig_ble();

/**
 * @brief Config the BLE and Enable
 *
 * @param[out] bt_mac Bluetooth's device (MAC) address (6 bytes)
 *
 * @return  ESP_OK if all steps complete, otherwise show the error message and abort the process.
 *
 **/
core_err ble_platform_enable_ble(uint8_t *bt_mac);

/**
 * @brief deConfig the BLE and disable
 *
 * @return  ESP_OK if all steps complete, otherwise show the error message and abort the process.
 *
 **/
core_err ble_platform_disable_ble();

void ble_platform_set_ble_dt_recv_q(QueueHandle *recv_queue) ;


/**
 * @brief
 *
 * @return
 *
 **/
void ble_platform_set_isble_connected(BLE_CONNECTION_STATUS status);


/**
 * @brief
 *
 * @return
 *
 **/
BLE_CONNECTION_STATUS ble_platform_isble_connected();

/**
 * @brief
 *
 * @return
 *
 **/
core_err ble_platform_set_connect_event_handler(BLE_CONNECT_EVENT handler);

#endif /* SRC_CONN_BLE_ESP_IDF_BLE_PLATFORM_H_ */
