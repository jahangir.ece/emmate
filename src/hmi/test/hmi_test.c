/*
 * File Name: hmi_test.c
 * File Path: /emmate/src/hmi/test/hmi_test.c
 * Description:
 *
 *  Created on: 11-May-2019
 *      Author: Noyel Seth
 */


#include "hmi_test.h"

#if TESTING

#include "system.h"
#include "hmi.h"

static void hmi_btn_event_handler(uint8_t io_pin, uint8_t state) {
	printf("hmi_btn_event_handler GPIO_IO_INTERRUPT= %d, state =%d\r\n", io_pin, state);
}


void init_touch_btn(){
	ButtonData btn_dt;
	btn_dt.io_pin = BSP_BTN_2;
	btn_dt.io_pulldown_mode=GPIO_IO_PULLDOWN_ENABLE;
	btn_dt.io_pullup_mode=GPIO_IO_PULLUP_DISABLE;
	btn_dt.int_type=GPIO_INTERRUPT_POSEDGE;
	btn_dt.notify_btn_stateCb = hmi_btn_event_handler;
	core_err ret = init_btn_interface(&btn_dt);
	if(ret!=CORE_OK){
		CORE_LOGE("hmi_test", "Failed to %s", __func__);
	}
}

void init_unknown_btn(){
	ButtonData btn_dt;
	btn_dt.io_pin = 5;
	btn_dt.io_pulldown_mode=GPIO_IO_PULLDOWN_ENABLE;
	btn_dt.io_pullup_mode=GPIO_IO_PULLUP_DISABLE;
	btn_dt.int_type=GPIO_INTERRUPT_POSEDGE;
	btn_dt.notify_btn_stateCb = hmi_btn_event_handler;
	core_err ret = init_btn_interface(&btn_dt);
	if(ret!=CORE_OK){
		CORE_LOGE("hmi_test", "Failed to %s", __func__);
	}
}

#endif
