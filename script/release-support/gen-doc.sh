#!/bin/bash

# setting the extglob
shopt -s extglob

echo "#####################################################################"
echo Generating doxygen docs

# creating the distribution doc directory
DIST_DOC_DIR=$ACTUAL_DIST_DIR/docs
mkdir -p $DIST_DOC_DIR

# creating temp src directory for generating docs
DIST_TEMP_DOC_SRC_DIR=$DIST_DOC_DIR/tmpsrc
mkdir -p $DIST_TEMP_DOC_SRC_DIR

# first copy the emmate index page doc and related files
DIST_TEMP_MAINPAGE_DIR=$PROJ_DIR/res/setup/files/emmate-docs/.
cp -v -r $DIST_TEMP_MAINPAGE_DIR $DIST_TEMP_DOC_SRC_DIR

# goto source directory
cd $SRC_DIR

# Copying the core src files
echo .....................................................................
echo

#creating temp src core directory
DIST_TEMP_DOC_SRC_CORE_DIR=$DIST_TEMP_DOC_SRC_DIR/core
mkdir -p $DIST_TEMP_DOC_SRC_CORE_DIR

echo Copying core src files from $SRC_DIR to $DIST_TEMP_DOC_SRC_CORE_DIR
cp -v -r !(platform) $DIST_TEMP_DOC_SRC_CORE_DIR


#Copying the platform level src files
echo .....................................................................
echo

source $CONFIG_PATH

#checking requested platforms
echo Checking requested platforms
echo PLATFORM = $PLATFORM

platform_count=0

if [ ! -z "$PLATFORM" ]
then
	platform_count=${#PLATFORM[@]}
	echo Number of requested platforms = $platform_count
else
	echo No platforms were requested
fi

if [ $platform_count -le 0 ]
then
	echo Setting all platforms
	cd $SRC_DIR/platform
	echo */
	PLATFORM=(*/)
	echo PLATFORM = ${PLATFORM[@]}
	platform_count=${#PLATFORM[@]}
	echo Generating release for $platform_count platforms
fi

if [ $platform_count -gt 0 ]
then
	for ((i=0; i<$platform_count; i++))
	do
		echo
		echo .....................................................................

		THIS_PLATFORM=${PLATFORM[$i],,}
		echo Generating release package for $THIS_PLATFORM...
		
		#checking the availability of the mentioned platform
		#goto platform source directory ie, src/platform/this_platform
		PLATFORM_SRC_DIR=$/platform/$THIS_PLATFORM
		{
			cd $PLATFORM_SRC_DIR
		} || {
			echo platform src directory NOT FOUND for $THIS_PLATFORM
			echo $PLATFORM_SRC_DIR NOT FOUND
			continue
		}

		#platform src directory found
		echo $PLATFORM_SRC_DIR FOUND
		
		#goto platform source directory
		cd $PLATFORM_SRC_DIR

		#Copying the platform src files

		#creating temp src platform directory
		DIST_TEMP_DOC_SRC_PLATFORM_DIR=$DIST_TEMP_DOC_SRC_DIR/$THIS_PLATFORM
		mkdir -p $DIST_TEMP_DOC_SRC_PLATFORM_DIR

		echo Copying platform src files from $PLATFORM_SRC_DIR to $DIST_TEMP_DOC_SRC_PLATFORM_DIR
		cp -v -r !(platform) $DIST_TEMP_DOC_SRC_PLATFORM_DIR

		echo .....................................................................
		echo
	done
fi

# Copying the platform level src files
echo .....................................................................
echo

'''
# goto platform source directory
EXAMPLES_DIR=$PROJ_DIR/examples
cd $EXAMPLES_DIR

# Copying the examples src files

# creating temp src examples directory
DIST_TEMP_DOC_SRC_EXAMPLES_DIR=$DIST_TEMP_DOC_SRC_DIR/examples
mkdir -p $DIST_TEMP_DOC_SRC_EXAMPLES_DIR

echo Copying examples src files from $EXAMPLES_DIR to $DIST_TEMP_DOC_SRC_EXAMPLES_DIR
cp -v -r !(platform) $DIST_TEMP_DOC_SRC_EXAMPLES_DIR
'''

# Generating temporary doxy_support.h in all directories
echo
echo .....................................................................
echo

doc_dirs=($(find $DIST_TEMP_DOC_SRC_DIR -type d))

for doc_dir in ${doc_dirs[@]}
do
	echo "//temporary doxy_support.h" > $doc_dir/doxy_support.h
done

echo
echo .....................................................................
echo


# example of directory structure in tmpsrc and to be generated doxygen groups
# core
#	|---- hmi
#	|		|---- hmi.h (defgroup-> CORE_HMI)
#	|		|
#	|		|---- led
#	|		|		|---- led_helper.h		(defgroup-> CORE_HMI_LED, ingroup-> CORE_HMI)
#	|		|		|---- led_patterns.h	(defgroup-> CORE_HMI_LED, ingroup-> CORE_HMI)
#	|		|
#	|		|---- button
#	|				|---- button_helper.h	(defgroup-> CORE_HMI_BUTTON, ingroup-> CORE_HMI)
#	|
# esp32
#	|---- hmi
#	|		|---- led
#	|		|		|---- led_platform.h	(defgroup-> ESP32_HMI_LED, ingroup-> CORE_HMI_LED)
#	|		|
#	|		|---- button
#	|				|---- button_platform.h	(defgroup-> ESP32_HMI_BUTTON, ingroup-> CORE_HMI_BUTTON)
#	|
# pic32
#	|---- hmi
#	|		|---- led
#	|		|		|---- led_platform.h	(defgroup-> PIC32_HMI_LED, ingroup-> CORE_HMI_LED)
#	|		|
#	|		|---- button
#	|				|---- button_platform.h	(defgroup-> PIC32_HMI_BUTTON, ingroup-> CORE_HMI_BUTTON)
#	.
#	.
#	.

#/** @defgroup group2 The Second Group
# *  @ingroup group1
# *  Group 2 is a subgroup of group 1
# */

#####################################################################
make_string_capital_case()
{
	for word in $1
	do
	    capital_case="$capital_case ${word^}"
	done
	echo $capital_case
}

echo
echo .....................................................................
echo

# https://www.geeksforgeeks.org/dos2unix-unix2dos-commands/
# https://unix.stackexchange.com/a/418066
# https://stackoverflow.com/a/28514531

echo Reading $CURR_DIR/release-support/doc-desc-words.conf file...

echo Processing dos2unix on $CURR_DIR/release-support/doc-desc-words.conf
dos2unix $CURR_DIR/release-support/doc-desc-words.conf
echo File converted to unix

actual_words=()
while IFS= read -r line || [ "$line" ]
do
	line="$line" | tr -d '\r' | tr -d '\n'
#	echo found ">>$line<<"
	if [[ "$line" ]] && [[ "$line" != \#* ]]
	then
		actual_words+=("$line")
	fi
done < $CURR_DIR/release-support/doc-desc-words.conf

actual_word_count=${#actual_words[@]}
echo ${actual_word_count} words found in the file...
echo
echo ${actual_words[@]}

echo
echo .....................................................................
echo

# DO NOT echo anything in this function
# echo from a function is returned to its caller
# Only the last echo is needed
make_string_actual_case()
{
	actual_case=""
	for word in $1
	do
#		echo "word:>>$word<<"
		for ((i=0; i<$actual_word_count; i++))
		do
			this_actual_word=${actual_words[$i]}
#			echo "actual_word:>>$this_actual_word<<"
			
			# need to match ignore case
#			echo ">>${word,,}<<>>${this_actual_word,,}<<"
			if [[ ${word,,} = ${this_actual_word,,} ]]
			then
#				echo matched
				word="$this_actual_word"
			fi
		done
	    actual_case="$actual_case $word"
	done
	echo $actual_case
}
#####################################################################

newline=$'\n'

# Creating array of header files for document generation
header_files=($(find $DIST_TEMP_DOC_SRC_DIR -name '*.h'))

header_count=${#header_files[@]}
echo Processing $header_count files
echo

for header_file in ${header_files[@]}
do
	echo .....................................................................
	echo
	echo Processing $header_file

#	echo Root path = $DIST_TEMP_DOC_SRC_DIR/
#	echo Removing the root path
	stripped_header_file=${header_file##$DIST_TEMP_DOC_SRC_DIR/}
	echo File name = $stripped_header_file
	
	module_name=$(dirname $stripped_header_file)
	echo Module name = $module_name
	
	group_name=${module_name//"/"/"_"}
	echo Group Name = $group_name
	
	#Checking if group name is more than one folder deep
	#We can do this by checking the number of _ (underscore) in the name
	underscores="${group_name//[^_]}"
	underscore_count="${#underscores}"
	echo underscore_count=$underscore_count

	if [ "$underscore_count" -gt "0" ]
	then

		# ${string##substring}
		# Deleting longest match of substring ("_") from front of $group_name
		group_desc=${group_name##*_}
		
		# Removing hypen from hyphenated words in group desc
		group_desc=${group_desc//"-"/" "}
		
		echo Group Desc = $group_desc

		#strip the last _ part and get the ingroup value
		ingroup_name=${group_name%_*}
		echo In Group Name = $ingroup_name
		defgroup_str+=" * @ingroup ${ingroup_name}${newline}"
	else
		if [[ $group_name == core* ]] || [[ $group_name == examples* ]]
		then
			group_desc=${group_name}
		fi
	fi
	
	if [[ $group_name == core* ]] || [[ $group_name == examples* ]]
	then
		echo This is a core src file
		
		#Setting defgroup value
		group_desc=`make_string_capital_case "${group_desc}"`
		group_desc=`make_string_actual_case "${group_desc}"`
		
		defgroup_str="/**${newline} * @defgroup ${group_name} ${group_desc}${newline}"

		if [ "$underscore_count" -gt "0" ]
		then
			#strip the last _ part and get the ingroup value
#			ingroup_name=${group_name%_*}
#			echo In Group Name = $ingroup_name
			defgroup_str+=" * @ingroup ${ingroup_name}${newline}"
		fi
		
		#Adding the comment closure
		defgroup_str+=" * @{${newline}"
		defgroup_str+=" */"

		echo
		echo "Doxygen Comment"
		echo "$defgroup_str"
		
		#Adding the doxygen comment to the top of the actual file
		echo "$defgroup_str"|cat - $header_file > /tmp/out && mv /tmp/out $header_file
	else
		echo This is a platform specific file
		
		# ${string%%substring}
		# Selecting shortest match of substring ("_") from front of $group_name to get platform name
		platform_name=${group_name%%_*}
		
		# Adding the platform name to the $group_desc
		group_desc="${platform_name} ${group_desc}"

		#Setting defgroup value
		group_desc=`make_string_capital_case "${group_desc}"`
		group_desc=`make_string_actual_case "${group_desc}"`

		defgroup_str="/**${newline} * @defgroup ${group_name} ${group_desc}${newline}"
		
		#Adding ingroup parameter
		ingroup_name=${group_name#*_}
		defgroup_str+=" * @ingroup core_${ingroup_name}${newline}"

		#Adding the comment closure
		defgroup_str+=" * @{${newline}"
		defgroup_str+=" */"

		echo
		echo "Doxygen Comment"
		echo "$defgroup_str"
		
		#Adding the doxygen comment to the top of the actual file
		echo "$defgroup_str"|cat - $header_file > /tmp/out && mv /tmp/out $header_file
	fi

	echo
	echo .....................................................................
done

echo
echo "#####################################################################"
echo

cd $DIST_TEMP_DOC_SRC_DIR
echo `pwd`
echo Running doxygen on $DIST_TEMP_DOC_SRC_DIR
echo .....................................................................
 
#Set the Doxygen file path. This path is inside the actual core source script/release-support dir
DOXYGEN_CONF_FILE=$CURR_DIR/release-support/core-doxy.conf
echo Using config file for doxygen: $DOXYGEN_CONF_FILE

#shopt -s nullglob

#for header_file in $(find . -name '*.h')
#do
#  echo "$header_file"
#done

doxygen $DOXYGEN_CONF_FILE >> $LOG_FILE

echo Moving docs to $ACTUAL_DIST_DIR
echo .....................................................................
cp -r ./docs $ACTUAL_DIST_DIR
#rm -rf $DIST_TEMP_DOC_SRC_DIR