#!/bin/bash

echo .....................................................................
echo Initiating release package build for $PROJ_NAME
echo .....................................................................
echo Build Mode             : $BUILD_MODE
echo Build Date             : $CURRENT_DATE
echo Build Time             : $CURRENT_TIME
echo Build By               : $USERNAME
echo .....................................................................
echo Project Location : $PROJ_DIR
echo .....................................................................
echo Distribution Location : $DIST_DIR
echo .....................................................................

