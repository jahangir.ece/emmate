/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_array_create.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a array object into a JSON using structure.
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for json_array_create module
 *
 */
void json_array_create_init();

/**
 * @brief	Execution function for json_array_create module
 *
 */
void json_array_create_loop();
