/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   nested_json_create.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a nested JSON using structure.
 *
 *
 *
 */

#ifndef NESTED_JSON_CREATE_H_
#define NESTED_JSON_CREATE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for nested_json_create module
 *
 */
void nested_json_create_init();

/**
 * @brief	Execution function for nested_json_create module
 *
 */
void nested_json_create_loop();

#endif	/* NESTED_JSON_CREATE_H_ */
