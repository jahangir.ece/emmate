/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   nested_json_create.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a nested JSON using structure.
 *
 *
 *
 */

#include "core_common.h"
#include "core_constant.h"
#include "system.h"

#include "core_utils.h"
#include "nested_json_create.h"

// Include JSON Library
#include "parson.h"

#define TAG	"nested_json_create"

/*
 *	Declaration of a global variables
 */
bool is_bool_val = true;
int integer_val = 1;
float float_val = 1;
double double_val = 1;

/*
 *	Declaration of a nested Example structure
 */
typedef struct {
	bool is_nested_bool_val;
	int nested_integer_val;
	float nested_float_val;
	double nested_double_val;
} NESTED_EXAMPLE_STRUCT;

/*
 *	Declaration of a Example structure
 */
typedef struct {
	char board_id[50];
	bool is_bool_val;
	int integer_val;
	float float_val;
	double double_val;
	NESTED_EXAMPLE_STRUCT nested_example_data;
} EXAMPLE_STRUCT;

/********************************************** Module's Static Functions **********************************************************************/

static core_err create_nested_json(JSON_Object *nested_root_object, NESTED_EXAMPLE_STRUCT *nested_example_data) {

	core_err ret = CORE_OK;

	if (json_object_set_boolean(nested_root_object, GET_VAR_NAME(nested_example_data->is_nested_bool_val, "->"),
			nested_example_data->is_nested_bool_val) != JSONSuccess) {
		// Nested Is-Bool-Value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(nested_root_object, GET_VAR_NAME(nested_example_data->integer_val, "->"),
			nested_example_data->nested_integer_val) != JSONSuccess) {
		// Nested Integer_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(nested_root_object, GET_VAR_NAME(nested_example_data->float_val, "->"),
			nested_example_data->nested_float_val) != JSONSuccess) {
		// Nested Float_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(nested_root_object, GET_VAR_NAME(nested_example_data->double_val, "->"),
			nested_example_data->nested_double_val) != JSONSuccess) {
		// Nested Double_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (ret == CORE_FAIL) {

	} else {
		ret = CORE_OK;
	}

	return ret;
}

static core_err create_json() {
	core_err ret = CORE_OK;
	char *your_thing_name = NULL;

	// populate the example_struct_data with data
	EXAMPLE_STRUCT example_struct_data = { .board_id = "board-001", .is_bool_val = is_bool_val, .integer_val =
			integer_val, .float_val = float_val, .double_val = double_val, .nested_example_data.is_nested_bool_val =
			is_bool_val, .nested_example_data.nested_integer_val = integer_val, .nested_example_data.nested_float_val =
			float_val, .nested_example_data.nested_double_val = double_val, };

	// Store JSON string into this variable.
	char *serialized_string = NULL;
	char *serialized_string_pretty = NULL;

	// Creating JSON root obj
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);

	if (json_object_set_string(root_object, GET_VAR_NAME(your_thing_name, NULL),
	YOUR_THING_NAME) != JSONSuccess) {
		// YOUR_THING_NAME failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_string(root_object, GET_VAR_NAME(example_struct_data.board_id, "."),
			example_struct_data.board_id) != JSONSuccess) {
		// Board-id failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_boolean(root_object, GET_VAR_NAME(example_struct_data.is_bool_val, "."),
			example_struct_data.is_bool_val) != JSONSuccess) {
		// Is-Bool-Value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.integer_val, "."),
			example_struct_data.integer_val) != JSONSuccess) {
		// Integer_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.float_val, "."),
			example_struct_data.float_val) != JSONSuccess) {
		// Float_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.double_val, "."),
			example_struct_data.double_val) != JSONSuccess) {
		// Double_value failed add into the root object
		ret = CORE_FAIL;
	}

	// Creating Nested JSON root obj
	JSON_Value *nested_root_value = json_value_init_object();
	JSON_Object *nested_root_object = json_value_get_object(nested_root_value);

	if (create_nested_json(nested_root_object, &example_struct_data.nested_example_data) == CORE_OK) {
		if (json_object_set_value(root_object, GET_VAR_NAME(example_struct_data.nested_example_data, "."),
				nested_root_value) != CORE_OK) {
			// Nested Json data failed add into the root object
			ret = CORE_FAIL;
		}
	}

	// Get The JSON in serialized format
	serialized_string = json_serialize_to_string(root_value);
	size_t serialized_string_len = json_serialization_size(root_value);

	// Get The JSON in serialized pretty format
	serialized_string_pretty = json_serialize_to_string_pretty(root_value);
	size_t serialized_string_pretty_len = json_serialization_size_pretty(root_value);

	// Consol print of Serialized_string
	if (serialized_string != NULL) {
		CORE_LOGI(TAG, "Serialized JSON string's length: %d \r\n Serialized String: %s \r\n", serialized_string_len,
				serialized_string);
		json_free_serialized_string(serialized_string);
	}

	// Consol print of Serialized_string Pretty
	if (serialized_string_pretty != NULL) {
		CORE_LOGI(TAG, "Serialized Pretty JSON string's length: %d \r\n Pretty String: %s \r\n\r\n",
				serialized_string_pretty_len, serialized_string_pretty);
		json_free_serialized_string(serialized_string_pretty);
	}

	if (ret == CORE_FAIL) {
		/* cleanup JSON memory allocation*/
		json_value_free(root_value);
	} else {
		json_value_free(root_value);
		ret = CORE_OK;
	}

	integer_val += 10;
	float_val += 10.25;
	double_val += 100.5;
	is_bool_val ^= true;

	return ret;
}

/*******************************************************************************************************************/

void nested_json_create_init() {
	CORE_LOGI(TAG, "In %s", __func__);
	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
}

void nested_json_create_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	// call create_json() to create the JSON
	create_json();
}
