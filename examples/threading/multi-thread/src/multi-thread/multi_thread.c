/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task
 *
 *
 *
 */

#include "multi_thread.h"

#define TAG	"multi_thread"

ThreadHandle thread_handle_1;
ThreadHandle thread_handle_2;

/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 3 */
static void task_1(void * param) {
	uint8_t count = 0;
	while (1) {
		count++;
		CORE_LOGI(TAG, "Hello %s running count %d..", __func__, count);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if (count > 100) {
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/* Task2 with priority 3 */
static void task_2(void * param) {
	uint8_t count = 0;
	while (1) {
		count++;
		CORE_LOGI(TAG, "Hello %s running count %d..", __func__, count);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if (count > 150) {
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/**************************************************************************************************************************/

void multi_thread_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 3 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 3 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_2.....");
	}

}

void multi_thread_loop() {
	//CORE_LOGI(TAG, "In %s", __func__);
}
