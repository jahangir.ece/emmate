# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="../fritzing/threading.png" width="700">

[//]: ![image](../fritzing/threading.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `threading` module APIs.

This examples does the following things:

- Creates 3 threads (tasks) `TaskCreate()` function
- Loops the threads (tasks) for n times
- Destroys the threads (tasks) by calling `TaskDelete()`