/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   hello_world.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to 
 * Getting Started
 *
 *
 *
 */
#include "hello_world.h"

void hello_world() {
	printf("%s\n", "Hello World");
}
