# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="fritzing/esp32_rtc_module.png" width="700">

[//]: ![image](fritzing/esp32_rtc_module.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the use of `peripherals/i2c` module APIs. It uses a RTC chip (DS1307) to read & write data. The `SDA_PIN` & `SCL_PIN` (i2c pins) are defined in `thing/thing.h`

This example does the following:

- Initializes an i2c master client using `init_i2c_core_master()` function
- Writes a hardcoded date in the RTC chip. To change the date you need to change this line in `rtc-module/rtc-module.c`: `uint8_t data_wr[7] = { 0x16, 0x12, 0x05, 0x06, 0x15, 0x06, 0x19 };`
- Read the RTC chip and prints the date time
