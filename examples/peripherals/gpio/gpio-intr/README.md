# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

<img src="fritzing/gpio-intr.jpg" width="700">

[//]: ![image](fritzing/gpio-intr.jpg)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates GPIO interrupt functionality using `peripherals/gpio` APIs. It uses a switch and a LED for the demonstration. To change the pins you need to change `SWITCH_2_GPIO` & `LED_3_GPIO` in `thing/thing.h`

This example does the following things:

- Configures a switch as GPIO input having positive edge interrupt
- Configures a LED as GPIO output
- Whenever the switch is pushed,the LED is toggled