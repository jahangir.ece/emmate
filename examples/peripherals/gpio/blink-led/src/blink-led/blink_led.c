/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   blink_led.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Blink of a LED
 *
 *
 *
 */

#include "blink_led.h"
#include "core_common.h"
#include "core_constant.h"
#include "system.h"

#include "gpio_core.h"

#define TAG "blink_led"

#define BLINK_LED_GPIO CUSTOM_LED_1

/********************************************** Module's Static Functions **********************************************************************/

/*******************************************************************************************************************/

void blink_led_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* 	Configure the IOMUX register for pad BLINK_LED_GPIO (some pads are
	 muxed to GPIO on reset already, but some default to other
	 functions and need to be switched to GPIO. Consult the
	 Technical Reference for a list of pads and their default
	 functions.)
	 */
	gpio_io_pad_select_gpio(BLINK_LED_GPIO);

	// Initialize the GPIO to Output mode
	gpio_io_set_direction(BLINK_LED_GPIO, GPIO_IO_MODE_OUTPUT);
}

void blink_led_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	static uint32_t bit = 0;
	bit ^= 1;

	// set the GPIO pin value to Glow (1) or OFF (0) to the LED
	gpio_io_set_level(BLINK_LED_GPIO, bit);
}
