#include "clcd_overwrite_string.h"

#define TAG	"core_app_main"

void core_app_main(void *param) {
	CORE_LOGI(TAG,
			"==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG,
			"Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG,
			"==================================================================");

	//initialize My module
	clcd_display_init();


	while (1) {
		// Execute My module
		clcd_display_string();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
