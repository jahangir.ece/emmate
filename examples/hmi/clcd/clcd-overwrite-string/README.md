# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so. 

The required components and tools are - 

1. 4x20 LCD display.
2. Single strand wires.
3. Breadboard.
4. Power Source for power up the module/dev-board.

Connect the Hardware as per the below diagram :

** For ESP32 DevKit-C V4 **

<img src="fritzing/lcd fritzing.png" width="700">

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `hmi/clcd` module APIs to initialize an LCD display.

This example shows:

- How to initialize a 4x20 LCD display.
- How to set string for displaying characters in multiple line.
- How to overwrite string in same row.
