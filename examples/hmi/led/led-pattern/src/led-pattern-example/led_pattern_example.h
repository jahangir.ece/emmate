/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   led_pattern.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The a LED Pattern of police van in different pattern using led-helper lib
 *
 *
 *
 */
#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"
#include "threading.h"
#include "hmi.h"
#include "thing.h"

// Pattern-1 For Mono Led, Led On for 1000msec, Off for 1000msec and this pattern run for 1 time
#define    LED_PATTERN1 	make_led_pattern(COLOR_MONO, 1000,1000, 1)

// Pattern-1 For Mono Led, Led On for 1msec, Off for 1000msec and this pattern run for 1 time
#define    LED_PATTERN2 	make_led_pattern(COLOR_MONO, 1, 1000 , 1)

// Pattern-1 For Mono Led, Led On for 1000msec, Off for 1msec and this pattern run for 1 time
#define    LED_PATTERN3  	make_led_pattern(COLOR_MONO, 1000, 1, 1)

// Pattern-1 For Mono Led, Led On for 1msec, Off for 2000msec and this pattern run for 1 time
#define    LED_PATTERN4  	make_led_pattern(COLOR_MONO, 1, 1000*2, 1)

// Pattern-1 For Mono Led, Led On for 2000msec, Off for 1000msec and this pattern run for 1 time
#define    LED_PATTERN5  	make_led_pattern(COLOR_MONO, 1000*2, 1, 1)

// Pattern-1 For Mono Led, Led On for 1000msec, Off for 2000msec and this pattern run for 1 time
#define    LED_PATTERN6  	make_led_pattern(COLOR_MONO, 1000, 1000*2, 1)

// Pattern-1 For Mono Led, Led On for 1000msec, Off for 1msec and this pattern run for 1 time
#define    LED_PATTERN7  	make_led_pattern(COLOR_MONO, 1000, 1, 1)

/**
 * @brief	Init function for led_pattern module
 *
 */
void led_pattern_init();

/**
 * @brief	Execution function for led_pattern module which generate police van led pattern
 *
 */
void led_pattern_loop();
